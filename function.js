function getData(model, res) {
  model.find({}, (err, collection) => {
    if (err) {
      res.status(400).send(err["message"]);
    } else {
      res.status(200).json(collection);
    }
  });
}

function postData(model, res, req) {
  model.create(req.body, (err, data) => {
    if (err) {
      res.status(400).send(err["message"]);
    } else {
      res.status(201).send(data);
    }
  });
}

function getDataById(model, res, req) {
  model.find({ _id: req.params.id }, (err, data) => {
    if (err) {
      res.status(400).send(err["message"]);
    } else {
      res.status(200).json(data);
    }
  });
}

function putDataById(model, res, req) {
  model.findByIdAndUpdate(req.params.id, { $set: req.body }, (err, data) => {
    if (err) {
      res.status(400).send(err["message"]);
    } else {
      getDataById(model, res, req);
    }
  });
}

function delDataById(model, res, req) {
  model.findOneAndRemove({ _id: req.params.id }, (err, data) => {
    if (err) {
      res.status(400).send(err["message"]);
    } else {
      res.status(200).send("data destroyed");
    }
  });
}

module.exports = {
  getData: getData,
  postData: postData,
  getDataById: getDataById,
  putDataById: putDataById,
  delDataById: delDataById
};
