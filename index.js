const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/movie-rental", { useNewUrlParser: true });
var db = mongoose.connection;
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
//const pretty = require("express-prettify");
const movieRouter = require("./routes/movie.js");
const genreRouter = require("./routes/genre.js");
const customerRouter = require("./routes/customer.js");
const rentalRouter = require("./routes/rental.js");

//view engine
// app.set("view engine", "ejs");
// app.set("views", path.join(__dirname, "views"));

//Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false })); //extended true deep parsing and extended false shallow parsing????
app.use(movieRouter);
app.use(genreRouter);
app.use(customerRouter);
app.use(rentalRouter);
//app.use(pretty({ query: "pretty" }));

//Event listner
db.once("open", () => {
  console.log("Connection has been made");
}).on("error", err => {
  console.log("error", err);
});

app.listen(3002);
