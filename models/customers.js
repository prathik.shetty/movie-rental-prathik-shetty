const mongoose = require("mongoose");
const schema = mongoose.Schema;

const customerSchema = new schema({
  name: {
    type: String,
    required: [true, "Enter Customer Name"],
    minlength: [1, "Type something"],
    maxlength: [10, "max of 10 characters"]
  },
  isPremium: {
    type: Boolean,
    required: [true, "Enter isPremium"],
    minlength: [1, "Type something"],
    maxlength: [10, "max of 10 characters"]
  },
  phone: {
    type: String,
    required: [true, "Enter Phone Number"],
    minlength: [1, "Type something"],
    maxlength: [10, "max of 10 characters"]
  }
});

module.exports = mongoose.model("customers", customerSchema);
