const mongoose = require("mongoose");
const schema = mongoose.Schema;

const genreSchema = new schema({
  genre: {
    type: String,
    required: [true, "Enter Genre"],
    minlength: [1, "Type something"],
    maxlength: [10, "max of 10 characters"]
  }
});
module.exports = mongoose.model("genres", genreSchema);
