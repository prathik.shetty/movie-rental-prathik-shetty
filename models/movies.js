const mongoose = require("mongoose");
const schema = mongoose.Schema;

const moviesSchema = new schema({
  title: {
    type: String,
    required: [true, "Enter Movie Name"],
    minlength: [1, "Type something"]
    //maxlength: [10, "max of 10 characters"]
  },
  genre: {
    type: String,
    required: [true, "Enter Genre"],
    minlength: [1, "Type something"],
    maxlength: [10, "max of 10 characters"]
  },
  numberInStock: {
    type: Number,
    required: [true, "Enter number in stock"],
    minlength: [1, "Type something"],
    maxlength: [10, "max of 10 characters"]
  },
  dailyRentalRate: {
    type: Number,
    required: [true, "Enter daily renatal rate"],
    minlength: [1, "Type something"],
    maxlength: [10, "max of 10 characters"]
  }
});
module.exports = mongoose.model("movies", moviesSchema);
