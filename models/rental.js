const mongoose = require("mongoose");
const schema = mongoose.Schema;

const rentalSchema = new schema({
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "customers",
    required: [true, "Enter customer ID"],
    minlength: [1, "Type something"],
    maxlength: [10, "max of 10 characters"]
  },
  movie: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "movies",
    required: [true, "Enter movie ID"],
    minlength: [1, "Type something"],
    maxlength: [10, "max of 10 characters"]
  },
  dateIssued: {
    type: Date,
    default: Date.now()
    //required: [true, "Enter date issued"]
  },
  dateReturned: {
    type: Date,
    minlength: [1, "Type something"]
    //required: [true, "Enter date returned"]
  },
  rentalFee: {
    type: Number,
    minlength: [1, "Type something"]
    //required: [true, "Enter rental fee"]
  }
});
module.exports = mongoose.model("rentals", rentalSchema);
