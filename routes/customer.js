const express = require("express");
const router = express.Router();
const func = require("/home/xteleon/MB/movie-rental/function.js");
const getData = func["getData"];
const postData = func["postData"];
const getDataById = func["getDataById"];
const putDataById = func["putDataById"];
const delDataById = func["delDataById"];
const customerModel = require("/home/xteleon/MB/movie-rental/models/customers.js");

router.get("/api/customers", (req, res) => {
  getData(customerModel, res);
});

router.post("/api/customers", (req, res) => {
  postData(customerModel, res, req);
});

router.get("/api/customers/:id", (req, res) => {
  getDataById(customerModel, res, req);
});

router.put("/api/customers/:id", (req, res) => {
  putDataById(customerModel, res, req);
});

router.delete("/api/customers/:id", (req, res) => {
  delDataById(customerModel, res, req);
});

module.exports = router;
