const express = require("express");
const router = express.Router();
const func = require("/home/xteleon/MB/movie-rental/function.js");
//const getData = func["getData"];
const postData = func["postData"];
//const getDataById = func["getDataById"];
const putDataById = func["putDataById"];
const delDataById = func["delDataById"];
const genreModel = require("/home/xteleon/MB/movie-rental/models/genre.js");
const movieModel = require("/home/xteleon/MB/movie-rental/models/movies.js");

function getDataGenre(model, res, req) {
  model.find({ genre: req.params.id }, (err, data) => {
    if (err) {
      res.send(err["message"]);
    }
    res.send(data);
  });
}

router.get("/api/genres", (req, res) => {
  //getData(genreModel, res);
  //getDataGenre(movieModel, res, req);
  movieModel.find().distinct("genre", (err, genre) => {
    res.send(genre);
  });
});

router.post("/api/genres", (req, res) => {
  //where should i post to genre collection or movie??
  postData(genreModel, res, req);
});

router.get("/api/genres/:id", (req, res) => {
  //getDataById(genreModel, res, req);
  getDataGenre(movieModel, res, req);
});

router.put("/api/genres/:id", (req, res) => {
  putDataById(genreModel, res, req);
});

router.delete("/api/genres/:id", (req, res) => {
  delDataById(genreModel, res, req);
});

module.exports = router;
