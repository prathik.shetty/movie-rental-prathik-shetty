const express = require("express");
const router = express.Router();
const func = require("/home/xteleon/MB/movie-rental/function.js");
const getData = func["getData"];
const postData = func["postData"];
const getDataById = func["getDataById"];
const putDataById = func["putDataById"];
const delDataById = func["delDataById"];
const movieModel = require("/home/xteleon/MB/movie-rental/models/movies.js");

router.get("/api/movies", (req, res) => {
  getData(movieModel, res);
});

router.post("/api/movies", (req, res) => {
  postData(movieModel, res, req);
});

router.get("/api/movies/:id", (req, res) => {
  getDataById(movieModel, res, req);
});

router.put("/api/movies/:id", (req, res) => {
  putDataById(movieModel, res, req);
});

router.delete("/api/movies/:id", (req, res) => {
  delDataById(movieModel, res, req);
});
module.exports = router;
