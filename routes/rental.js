const express = require("express");
const router = express.Router();
const func = require("/home/xteleon/MB/movie-rental/function.js");
// const getData = func["getData"];
const postData = func["postData"];
// const getDataById = func["getDataById"];
// const putDataById = func["putDataById"];
// const delDataById = func["delDataById"];
const rentalModel = require("/home/xteleon/MB/movie-rental/models/rental.js");
function getDataRent(model, res) {
  model
    .find()
    .populate("customer movie") // No clue what is happening....
    .exec((err, data) => {
      res.send(data);
    });
}

router.get("/api/rentals", (req, res) => {
  getDataRent(rentalModel, res);
});
router.post("/api/rentals", (req, res) => {
  postData(rentalModel, res, req);
});

module.exports = router;
